import './App.css';
import {Component} from "react";
import LotteryNumber from './LotteryNumber/LotteryNumber';
import {generateArray} from './NumberGenerator';

class App extends Component {
  state = {
    Numbers : []
  }
  render() {
    return (
        <div className="lottery">
          <div id='btnGenerate'>
            <button onClick={this.generateNumbers}>New numbers</button>
          </div>
          <div className="lotteryNumbers">
            <LotteryNumber number = {this.state.Numbers[0]}/>
            <LotteryNumber number = {this.state.Numbers[1]}/>
            <LotteryNumber number = {this.state.Numbers[2]}/>
            <LotteryNumber number = {this.state.Numbers[3]}/>
            <LotteryNumber number = {this.state.Numbers[4]}/>
          </div>
        </div>
    );
  }

  generateNumbers = () => {
    for(let i=0; i<500; i++){
      setTimeout(()=>{
        this.setState({Numbers: generateArray(5)});
      }, 500);
    }
  };
}

export default App;