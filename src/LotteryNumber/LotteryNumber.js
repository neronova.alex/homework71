import React from 'react';
import './LotteryNumber.css';

const LotteryNumber = props => {
    return (
        <div className="LotteryNumber">
            <h1>{props.number}</h1>
        </div>
    )
}

export  default LotteryNumber;